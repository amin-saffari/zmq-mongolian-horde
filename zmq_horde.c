/*
    Copyright (c) 2019 Udo Stenzel

    zmq-mongolian-horde is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    (LGPL) as published by the Free Software Foundation; either version
    3 of the License, or (at your option) any later version.

    As a special exception, the Contributors give you permission to link
    this library with independent modules to produce an executable,
    regardless of the license terms of these independent modules, and to
    copy and distribute the resulting executable under terms of your choice,
    provided that you also meet, for each linked independent module, the
    terms and conditions of the license of that module. An independent
    module is a module which is not derived from or based on this library.
    If you modify this library, you must extend this exception to your
    version of the library.

    zmq-mongolian-horde is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty
    of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

enum loudness { silent, warnings, debug } ;
static const enum loudness loudness = silent ;
static const unsigned int ring_size = 1U<<31 ;

#include <assert.h>
#include <malloc.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <zmq.h>

#include "zmq_horde.h"

//PRIu64 is a macro which internally translates to "lld" but not on all platforms.
#include <inttypes.h>

static const uint64_t rn_large = 1ULL << 63 ;
static inline int      leq( uint64_t a, uint64_t b ) { return b-a <  rn_large ; }
static inline int       lt( uint64_t a, uint64_t b ) { return a-b >= rn_large ; }

// A frame is a message part.  A singly linked list of frames is a complete message.
typedef struct frame {
    zmq_msg_t msg ;
    struct frame *next ;
} frame_t ;

static int message_send( frame_t *msg, void *sock ) 
{
    int rc = 0 ;
    while( msg ) {
        zmq_msg_t m ;
        zmq_msg_init(&m) ;
        zmq_msg_copy(&m, &msg->msg) ;
        rc = zmq_msg_send( &m, sock, msg->next ? ZMQ_SNDMORE : 0 ) ;
        if( rc == -1 ) break ;
        msg = msg->next ;
    }
    return rc ;
}

static void message_free( frame_t **pmsg )
{
    frame_t *p = *pmsg ;
    while( p ) {
        frame_t *p2 = p->next ;
        zmq_msg_close( &p->msg ) ;
        free( p ) ;
        p = p2 ;
    }
    *pmsg = 0 ;
}

static int message_recv( frame_t **pmsg, void *sock )
{
    for(;;) {
        *pmsg = malloc( sizeof(frame_t) ) ;
        zmq_msg_init( &(*pmsg)->msg ) ;
        int rc = zmq_msg_recv( &(*pmsg)->msg, sock, 0 ) ; 
        if( rc == -1 || !zmq_msg_more( &(*pmsg)->msg ) ) {
            (*pmsg)->next = 0 ;
            return rc ;
        }
        pmsg = &(*pmsg)->next ;
    }
}

int message_recv_and_discard( void *sock )
{
    int rc ;
    zmq_msg_t m ;
    zmq_msg_init( &m ) ;
    do {
        rc = zmq_msg_recv( &m, sock, 0 ) ;
    } while( rc != -1 && zmq_msg_more( &m ) ) ;
    zmq_msg_close( &m ) ;
    return rc ;
}

int streq( zmq_msg_t *m, const char *s )
{
    return zmq_msg_size(m) == strlen(s)
        && strncmp( s, zmq_msg_data(m), zmq_msg_size(m) ) == 0 ;
}

int zmq_horde( void* upstream, void* downstream, void* horde, void* control )
{
    int linger = 0 ;
    zmq_setsockopt( upstream, ZMQ_LINGER, &linger, sizeof(linger) ) ;
    zmq_setsockopt( downstream, ZMQ_LINGER, &linger, sizeof(linger) ) ;
    zmq_setsockopt( horde, ZMQ_LINGER, &linger, sizeof(linger) ) ;
    if( control ) zmq_setsockopt( control, ZMQ_LINGER, &linger, sizeof(linger) ) ;

    int is_paused = 0 ;
    int rc = 0 ;

    // We use an array of messages as a ring buffer.  Null pointers are
    // empty slots.  Otherwise, the first frame contains the sequence
    // number.  Replies have bit 63 in their sequence number set.
    // We track the buffer's state through pointers:
    //
    // - s.num_requests: the first free slot
    // - next_request: the first request not yet processed
    // - s.num_replies: the first reply to send downstream
    // - next_send: next request to send for the first time
    // - next_resend: the next request to resend
    //
    // This divides the ring buffer into four areas:  free area (from
    // s.num_requests to s.num_replies), requests only (from next_send to
    // s.num_requests), mixed requests and replies (from next_request to
    // next_send), replies only (from s.num_replies to next_request).  The
    // next_resend marker might travel over the mixed area repeatedly.
    
    frame_t **ringbuf = calloc( ring_size, sizeof(frame_t*) );
    uint8_t *is_reply = calloc( ring_size, 1 ) ;

    struct zmq_horde_stats s = {
        .current_requests = 0,
        .current_replies = 0,
        .current_free = ring_size,
        .num_replies = 0,
        .num_requests = 0,
        .total_resends = 0,
        .total_dups = 0
    } ;

    // bookkeeping pointers
    uint64_t next_request = 0, next_send = 0, next_resend = 0 ;

    zmq_pollitem_t polls[] = {
        { socket : downstream, fd : 0, events : 0, revents : 0 },
        { socket :      horde, fd : 0, events : 0, revents : 0 },
        { socket :   upstream, fd : 0, events : 0, revents : 0 },
        { socket :    control, fd : 0, events : ZMQ_POLLIN, revents : 0 }
    } ;

    for(;;) {
        assert( s.current_replies + s.current_requests + s.current_free == ring_size ) ;     // accounting adds up
        assert( s.num_requests - s.num_replies == s.current_replies + s.current_requests ) ; // space sizes add up

        assert( leq(s.num_replies,next_request) ) ;                                  // requests points after replies
        assert( leq(next_request,next_resend) ) ;                                    // resends points after requests
        assert( leq(next_resend,next_send) ) ;                                       // send points after resend
        assert( leq(next_send,s.num_requests) ) ;                                    // free points after send

        assert( next_request - s.num_replies <= s.current_replies ) ;                // output contains only replies
        assert( s.num_requests - next_send <= s.current_requests ) ;                 // send contains only requests
        assert( next_send - next_request <= s.current_replies+s.current_requests ) ; // mixed contains only reqs and reps
            
        if( loudness >= debug )
            fprintf( stderr, "next request: %"PRIu64", next free: %"PRIu64", next reply: %"PRIu64"\n"
                   , next_request, s.num_requests, s.num_replies ) ;

        // outgoing: poll for ZMQ_POLLOUT if we're holding onto a reply
        polls[0].events = !is_paused && s.num_replies != next_request ? ZMQ_POLLOUT : 0 ;

        // to horde: poll for ZMQ_POLLOUT if we're holding a request
        // from horde: always, and maybe only to throw it away
        polls[1].events = !is_paused && s.current_requests ? ZMQ_POLLOUT | ZMQ_POLLIN : ZMQ_POLLIN ;

        // incoming: only if we have room
        polls[2].events = !is_paused && s.current_free ? ZMQ_POLLIN : 0 ;

        rc = zmq_poll( polls, control ? 4 : 3, -1 ) ;
        if( rc < 0 ) break ; 

        // can send output downstream, so send one record
        if(polls[0].revents & ZMQ_POLLOUT) {
            assert( s.num_replies != next_request ) ;  // Why did I want to send output if nothing is done?

            // send message *without* sequence number
            rc = message_send( ringbuf[s.num_replies % ring_size]->next, downstream ) ;
            if( rc < 0 ) break ;

            // invalidate the slot so it doesn't get sent again.
            message_free( &ringbuf[s.num_replies % ring_size] ) ;
            is_reply[s.num_replies % ring_size] = 0 ;

            s.current_replies--;
            s.current_free++;
            s.num_replies++;
        }

        // Ready to send to the horde.  We send some unacknowledged request.
        if(polls[1].revents & ZMQ_POLLOUT) {
            assert( s.current_requests ) ;            // Why did I ask the horde for volunteers again?
            frame_t *msg ;
            if( next_send != s.num_requests ) {
                assert( !is_reply[next_send % ring_size] ) ;
                msg = ringbuf[next_send % ring_size] ;
                do {
                    next_send++;
                } while( next_send != s.num_requests && is_reply[next_send % ring_size] ) ;
            }
            else {
                while( is_reply[next_resend % ring_size] ) {
                    next_resend++;
                    if( next_resend == next_send ) next_resend = next_request ;
                }
                
                msg = ringbuf[next_resend % ring_size] ;
                s.total_resends++ ;

                do {
                    next_resend++;
                    if( next_resend == next_send ) next_resend = next_request ;
                }
                while( is_reply[next_resend % ring_size] ) ;
            }

            rc = message_send( msg, horde ) ;
            if( rc < 0 ) break ; 
        }

        // Something came back from the horde.  Make sure we actually want it, then store it.
        if(polls[1].revents & ZMQ_POLLIN) {
            zmq_msg_t msg ;
            zmq_msg_init( &msg ) ;
            rc = zmq_msg_recv( &msg, horde, 0 ) ;
            if( rc < 0 ) { zmq_msg_close( &msg ) ; break ; }
            
            if( !zmq_msg_more(&msg) ) {
                if( loudness >= debug )
                    fprintf( stderr, "[zmq_horde] received junk, %d bytes\n", rc ) ;
            }
            else if( zmq_msg_size(&msg) < 8 ) {
                if( loudness >= debug ) fprintf( stderr, "[zmq_horde] received junk, %d+ bytes\n", rc ) ;
                message_recv_and_discard( horde ) ;
            }
            else {
                uint64_t rn = *(uint64_t*)zmq_msg_data(&msg) ;
                if( loudness >= debug ) fprintf( stderr, "[zmq_horde] received record %"PRIu64".\n", rn ) ;
                if( lt(rn,next_request) ) {
                    if( loudness >= warnings ) fprintf( stderr, "[zmq_horde] this is old shit: %"PRIu64".\n", rn ) ;
                    s.total_dups++ ;
                    message_recv_and_discard( horde ) ;
                }
                else if( leq(next_send,rn) ) {
                    if( loudness >= warnings ) fprintf( stderr, "[zmq_horde] this comes from the future: %"PRIu64".\n", rn ) ;
                    s.total_dups++ ;
                    message_recv_and_discard( horde ) ;
                }
                else if( is_reply[rn % ring_size] ) {
                    if( loudness >= warnings ) fprintf( stderr, "[zmq_horde] this is a duplicate: %"PRIu64".\n", rn ) ;
                    s.total_dups++ ;
                    message_recv_and_discard( horde ) ;
                }
                else {
                    if( loudness >= debug ) fprintf( stderr, "this is not a duplicate, dealing with it\n" ) ;
                    is_reply[ rn % ring_size ] = 1 ;

                    message_free( &ringbuf[ rn % ring_size ] ) ;
                    ringbuf[ rn % ring_size ] = malloc( sizeof( frame_t ) ) ;
                    zmq_msg_init( &ringbuf[ rn % ring_size ]->msg ) ;
                    zmq_msg_copy( &ringbuf[ rn % ring_size ]->msg, &msg ) ;
                    message_recv( &ringbuf[ rn % ring_size ]->next, horde ) ;

                    s.current_requests-- ;
                    s.current_replies++ ;

                    if( rn == next_request ) {
                        while( next_request != next_send && is_reply[next_request % ring_size] )
                            next_request++;
                    }

                    if( lt(next_resend,next_request) ) next_resend = next_request ;
                    if( s.current_requests ) {
                        while( is_reply[next_resend % ring_size] ) {
                            next_resend++;
                            if( next_resend == next_send ) next_resend = next_request ;
                        }
                    }
                }
            }
            zmq_msg_close( &msg ) ;
        }

        // Input is available.  Give it the sequence number s.num_requests,
        // store it, and also mark it as the next thing to send.
        if( polls[2].revents & ZMQ_POLLIN) {
            if( loudness >= debug ) fprintf( stderr, "got input record.\n" ) ;

            assert( !ringbuf[s.num_requests % ring_size] ) ;     // slot should have been free
            ringbuf[s.num_requests % ring_size] = malloc( sizeof( frame_t ) ) ;
            zmq_msg_init_size( &ringbuf[s.num_requests % ring_size]->msg, 8 ) ;
            *(uint64_t*)zmq_msg_data(&ringbuf[s.num_requests % ring_size]->msg) = s.num_requests ;

            rc = message_recv( &ringbuf[s.num_requests % ring_size]->next, upstream ) ;
            if( rc < 0 ) break ;

            s.current_requests++ ;
            s.num_requests++;
            s.current_free-- ;
        }

        // a control message arrived
        if( polls[3].revents & ZMQ_POLLIN) {
            zmq_msg_t m ;
            zmq_msg_init( &m ) ;
            rc = zmq_msg_recv( &m, control, 0 ) ;
            if( rc<0 ) { zmq_msg_close(&m) ; break ; }

            if( streq( &m, "STATISTICS" ) || streq( &m, "TERMINATE" ) ) {
                rc = zmq_send( control, &s, sizeof( struct zmq_horde_stats ), 0 ) ;
                if( rc<0 || streq( &m, "TERMINATE" ) ) { zmq_msg_close(&m) ; break ; }
            }
            if( streq( &m, "PAUSE"  ) ) is_paused = 1 ;
            if( streq( &m, "RESUME" ) ) is_paused = 0 ;
            zmq_msg_close( &m ) ;
        }
    }
    
    for( unsigned int i = 0 ; i != ring_size ; ++i ) message_free( ringbuf+i ) ;
    free( is_reply ) ;
    free( ringbuf ) ;
    return rc ;
}
