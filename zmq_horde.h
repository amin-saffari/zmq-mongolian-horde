#ifndef INCLUDED_ZMQ_HORDE_H
#define INCLUDED_ZMQ_HORDE_H

struct zmq_horde_stats {
    uint64_t current_requests ; // number of requests currently queued
    uint64_t current_replies ;  // number of replies currenlty queued
    uint64_t current_free ;     // number of free slots in the buffer

    uint64_t num_replies ;      // number of replies sent
    uint64_t num_requests ;     // number of requests received

    uint64_t total_resends ;    // number of repeated requests sent
    uint64_t total_dups ;       // number of duplicate replies received
} ;

int zmq_horde( void* upstream, void* downstream, void* horde, void* control ) ;

#endif
