# zmq-mongolian-horde

Do you have an embarrassingly parallel computing problem?  Do you also
have a swarm of unreliable machine you want to distribute it to?  Then
you need The Mongolian Horde.

The Mongolian Horde is a *device* built on [ØMQ](https://zero.mq).  Its
frontend receives requests and sends corresponding replies in order and
asynchronously.  Thus it fits nicely into a pipeline communication
patterns.  The backend sends the requests to a number of worker nodes in
no particular order and buffers and reorders replies received from them.
If messages or whole worker nodes get lost, the device resends requests
and discards duplicate replies as needed.

See the man page for details and [demo.c](demo.c) for an example how to
use it.  zmq-mongolian-horde is licensed under the same terms as ØMQ
itself, see [zmq\_horde.h](zmq_horde.h) and COPYING.LESSER for details.

## How to use

The library is distributed as a single source file
([zmq\_horde.c](zmq_horde.c)) and a single header file
([zmq\_horde.h](zmq_horde.h)).  Compile the C file as part of your
project, and include the header wherever needed.

